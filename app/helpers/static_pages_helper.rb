module StaticPagesHelper
  def like? activity
    current_user.like activity.id
  end
end
